CREATE TABLE cover
(
id  int not null auto_increment primary key,
title varchar(50) not null,
description text not null,
image blob not null
);

CREATE TABLE store
(
id int not null auto_increment primary key,
cover_id in not null,
name varchar(50) not null,
image blob not null
);